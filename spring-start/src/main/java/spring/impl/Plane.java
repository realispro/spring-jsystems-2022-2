package spring.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import spring.Fast;
import spring.Person;
import spring.Transportation;

import javax.annotation.PostConstruct;

@Component("plane")
@Fast
@Scope("prototype")
@Lazy(true)
public class Plane implements Transportation {

    @Override
    public void transport(Person p) {
        System.out.println("Person " + p + " is being transported by plane");
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println("plane: post construct");
    }

}
