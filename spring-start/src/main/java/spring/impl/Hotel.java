package spring.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component("accomodation")
@PropertySource("/gratis.properties")
public class Hotel implements Accomodation {


    @Value("${gratis.meal:lody}")
    private String gratis;

    //@Autowired
    //@Qualifier("meals")
    @Resource
    private List<String> meals;

    @PostConstruct
    private void addGratis(){
        meals.add(gratis);
    }

    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }
}
