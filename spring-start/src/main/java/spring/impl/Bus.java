package spring.impl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import spring.Person;
import spring.Transportation;

@Component("bus")
@Primary
public class Bus implements Transportation {
    @Override
    public void transport(Person p) {
        System.out.println("person " + p + " is being transported by BUS");
    }

}
