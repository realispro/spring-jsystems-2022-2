package spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import spring.Person;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

@Aspect
@Component
public class TravelAspect {

    @Pointcut("execution(public * *(..))")
    void all(){}


    @Before("all()")
    void logEntering(JoinPoint jp){
        System.out.println("[entering] " + jp.toString() + " " + jp.getTarget().getClass().getSimpleName());
    }

    @Around("all()")
    Object timeMeasure(ProceedingJoinPoint jp) throws Throwable {
        LocalTime start = LocalTime.now();
        Object o = jp.proceed(jp.getArgs());
        LocalTime end = LocalTime.now();
        System.out.println("[around] " + jp.toString() + " " + jp.getTarget().getClass().getSimpleName()
                + " execution time: " + Duration.between(start, end).toNanos());

        return o;
    }


    @Before("execution(public * *(spring.Person))")
    void ticketValidation(JoinPoint jp){
        Person person = (Person) jp.getArgs()[0];
        if(person.getTicket().getValid().isBefore(LocalDate.now())){
            //System.out.println("[TICKET CONTROL FAILED]" + jp.toString());
            throw new RuntimeException("[TICKET CONTROL FAILED]" + jp.toString());
        } else {
            System.out.println("[TICKET CONTROL SUCCEEDED]" + jp.toString());
        }
    }
}
