package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@ComponentScan("spring")
@EnableAspectJAutoProxy
public class TravelConfig {

   /* @Bean
    public String name() {
        return "Business travel";
    }
*/
    @Bean
    public List<String> meals() {
        return new ArrayList<>(Arrays.asList("jajecznica", "kanapki", "kawa"));
    }
}
