package stellar.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class PlanetarySystem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String star;
    private Date discovery;
    private float distance;
    @Transient
    private URL details;

    @JsonIgnore
    @OneToMany(mappedBy = "system")
    private List<Planet> planets;

    public PlanetarySystem(int id, String name, String star, Date discovery, float distance, String url) {
        this.id = id;
        this.name = name;
        this.star = star;
        this.discovery = discovery;
        this.distance = distance;
        try {
            details = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "PlanetarySystem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", star='" + star + '\'' +
                '}';
    }
}
