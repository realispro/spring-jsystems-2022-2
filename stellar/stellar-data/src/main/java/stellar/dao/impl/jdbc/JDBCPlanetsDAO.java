package stellar.dao.impl.jdbc;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@RequiredArgsConstructor
public class JDBCPlanetsDAO implements PlanetDAO {

    public static final Logger logger = Logger.getLogger(JDBCPlanetsDAO.class.getName());

    public static final String SELECT_ALL_PLANETS = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p";

    public static final String SELECT_PLANETS_BY_SYSTEM = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM_AND_NAME = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=? and name like ?";

    public static final String SELECT_PLANET_BY_ID = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where id=?";



    private final JdbcTemplate jdbcTemplate;

    @Override
    public List<Planet> getAllPlanets() {
        List<Planet> planets = new ArrayList<>();
        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        List<Planet> systems = jdbcTemplate.query(SELECT_PLANETS_BY_SYSTEM, new PlanetMapper(), system.getId());

        return systems;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        try {
            return jdbcTemplate.queryForObject(SELECT_PLANET_BY_ID, new PlanetMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }

    }


    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }


    private Planet mapPlanet(ResultSet rs) throws SQLException {
        Planet p = new Planet();
        p.setId(rs.getInt("planet_id"));
        p.setName(rs.getString("planet_name"));
        p.setSize(rs.getInt("planet_size"));
        p.setWeight(rs.getInt("planet_weight"));
        p.setMoons(rs.getInt("planet_moons"));
        return p;
    }
}
