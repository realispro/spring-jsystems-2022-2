package stellar.dao.impl.data;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

@RequiredArgsConstructor
@Repository
@Primary
public class DataPlanetDAOAdapter implements PlanetDAO {

    private final PlanetRepository planetRepository;

    @Override
    public List<Planet> getAllPlanets() {
        return planetRepository.findAll();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return planetRepository.findAllBySystem(system);
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return planetRepository.findById(id).get();
    }

    @Override
    public Planet addPlanet(Planet p) {
        return planetRepository.saveAndFlush(p);
    }
}
