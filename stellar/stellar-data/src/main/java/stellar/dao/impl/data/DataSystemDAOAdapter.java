package stellar.dao.impl.data;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;

@RequiredArgsConstructor
@Repository
@Primary
public class DataSystemDAOAdapter implements SystemDAO {

    private final SystemRepository systemRepository;


    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return systemRepository.findAll();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return systemRepository.findAllByNameLike("%" + like + "%");
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return systemRepository.findById(id).get();
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        return systemRepository.saveAndFlush(system);
    }
}
