package stellar.dao.impl.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
public class JPAPlanetDAO implements PlanetDAO {

    @PersistenceContext(unitName = "stellar")
    private EntityManager em;

    @Override
    public List<Planet> getAllPlanets() {
        return null;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return null;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return null;
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }
}
