package stellar.dao.impl.jdbc;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
@RequiredArgsConstructor
public class JDBCSystemsDAO implements SystemDAO {

    public static final Logger logger = Logger.getLogger(JDBCSystemsDAO.class.getName());

    public static final String SELECT_ALL_SYSTEMS = "select ps.id as system_id, " +
            "ps.name as system_name, ps.distance as system_distance, ps.discovery as system_discovery from planetarysystem ps";

    public static final String SELECT_SYSTEMS_BY_NAME = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where name like ?";

    public static final String SELECT_SYSTEM_BY_ID = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where id=?";

    private final JdbcTemplate jdbcTemplate;
    //private final DataSource dataSource;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        List<PlanetarySystem> systems = jdbcTemplate.query(SELECT_ALL_SYSTEMS, new SystemMapper());
                /*new ArrayList<>();

        try(Connection connection = dataSource.getConnection();Statement stmt = connection.createStatement();){
            ResultSet rs = stmt.executeQuery(SELECT_ALL_SYSTEMS);
            while(rs.next()){
                systems.add(mapSystem(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        return systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        List<PlanetarySystem> systems = new ArrayList<>();

        return systems;
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {

        /*try(Connection c = dataSource.getConnection(); PreparedStatement stmt = c.prepareStatement(SELECT_SYSTEM_BY_ID)){

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                return mapSystem(rs);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        try {
            return jdbcTemplate.queryForObject(SELECT_SYSTEM_BY_ID, new SystemMapper(), id);
        }catch (EmptyResultDataAccessException e){
            logger.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {

        KeyHolder kh = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement ps = con.prepareStatement(
                                "INSERT INTO PLANETARYSYSTEM(NAME, STAR, DISTANCE, DISCOVERY) VALUES (?,?,?,?)",
                                new String[]{"id"});
                        ps.setString(1, system.getName());
                        ps.setString(2, system.getStar());
                        ps.setFloat(3, system.getDistance());
                        ps.setDate(4, new java.sql.Date(system.getDiscovery().getTime()));
                        return ps;
                    }
                },
                kh
        );
        system.setId(kh.getKey().intValue());

        return system;
    }

    public PlanetarySystem mapSystem(ResultSet rs) throws SQLException {
        PlanetarySystem ps = new PlanetarySystem();
        ps.setId(rs.getInt("system_id"));
        ps.setName(rs.getString("system_name"));
        ps.setDistance(rs.getFloat("system_distance"));
        ps.setDiscovery(rs.getDate("system_discovery"));
        return ps;
    }

}
