package stellar.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import stellar.dao.PlanetDAO;
import stellar.dao.SystemDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.logging.Logger;

@RequiredArgsConstructor
@Service
public class StellarServiceImpl implements StellarService {

    Logger logger = Logger.getLogger(StellarServiceImpl.class.getName());

    private final SystemDAO systemDAO;
    private final PlanetDAO planetDAO;

    @Override
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems =  systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        //logger.info("found: " + system);
        return  system;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system,like);
    }


    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.addPlanet(p);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {
       /* TransactionStatus ts = tm.getTransaction(new DefaultTransactionDefinition());
        try {*/
            PlanetarySystem system = systemDAO.addPlanetarySystem(s);
            if (s.getDistance() > 1000) {
                throw new IllegalArgumentException("to far!");
            }
            //tm.commit(ts);
            return system;
        /*}catch (IllegalArgumentException e){
            tm.rollback(ts);
            throw e;
        }*/
    }
}
