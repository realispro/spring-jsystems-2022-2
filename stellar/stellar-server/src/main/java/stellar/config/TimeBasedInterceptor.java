package stellar.config;

import lombok.Setter;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

@Setter
public class TimeBasedInterceptor implements HandlerInterceptor {

    private int opening;
    private int closing;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int hour = LocalTime.now().getHour();

        if(hour>=opening && hour<closing){
            return true;
        } else {
            response.setStatus(403);
            response.getWriter().println("<div>closed</div>");
            //response.sendRedirect("https://static4.depositphotos.com/1005534/377/v/450/depositphotos_3774838-stock-illustration-closed-neon-sign-rainbow-color.jpg");
            return false;
        }
    }
}
