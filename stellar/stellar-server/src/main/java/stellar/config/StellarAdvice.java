package stellar.config;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice(annotations = Adviced.class)
public class StellarAdvice {

    private static final Logger log = Logger.getLogger(StellarAdvice.class.getName());

    @ExceptionHandler(IllegalArgumentException.class)
    public String handleException(IllegalArgumentException e, Model model){
        log.log(Level.SEVERE, e.getMessage(), e);
        model.addAttribute("error_message", "[IllegalArgumentException] from  advice: " +e.getMessage());
        return "error";
    }


    @ExceptionHandler(Exception.class)
    public String handleException(Exception e, Model model){
        log.log(Level.SEVERE, e.getMessage(), e);
        model.addAttribute("error_message", "from  advice: " +e.getMessage());
        return "error";
    }
}
