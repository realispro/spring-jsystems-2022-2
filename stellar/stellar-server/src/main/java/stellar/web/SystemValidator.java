package stellar.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import stellar.model.PlanetarySystem;

@Component
public class SystemValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(PlanetarySystem.class);
    }

    @Override
    public void validate(Object target, Errors errors) {

        PlanetarySystem system = (PlanetarySystem) target;

        if(system.getName()==null || system.getName().trim().isEmpty()){
            errors.rejectValue("name", "errors.name.empty");
        }

        if(system.getDistance()<=0){
            errors.rejectValue("distance", "errors.distance.negative");
        }
    }
}
