package stellar.web.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stellar.model.Planet;
import stellar.service.StellarService;

import java.util.logging.Logger;

@RestController
@RequiredArgsConstructor
@RequestMapping("/webapi/planets")
public class PlanetRest {

    private final Logger log = Logger.getLogger(SystemRest.class.getName());
    private final StellarService service;

    @GetMapping("/{id}")
    public Planet getPlanet(@PathVariable("id") int id){
        Planet planet = service.getPlanetById(id);
        log.info("JWST targeting planet " + (planet!=null ? planet.getName() : "null"));
        return planet;
    }
}
