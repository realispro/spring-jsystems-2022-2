package stellar.web.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;
import stellar.web.SystemValidator;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequiredArgsConstructor
@RequestMapping("/webapi/systems")
public class SystemRest {

    private Logger log = Logger.getLogger(SystemRest.class.getName());

    private final StellarService service;
    private final SystemValidator validator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @GetMapping
    public List<PlanetarySystem> getSystems(@RequestParam(value = "phrase", required = false) String phrase){
        log.info("about to fetch all planetary systems");
        return phrase==null ? service.getSystems() : service.getSystemsByName(phrase);
    }

    @GetMapping("/{id}")
    public ResponseEntity getSystem(@PathVariable("id") int systemId) {
        log.info("about to fetch planetary system with id: " + systemId);
        PlanetarySystem p = service.getSystemById(systemId);
        return p != null ? ResponseEntity.ok(p) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Object not found.");
    }

    @GetMapping("/{id}/planets")
    public ResponseEntity getPlanets(@PathVariable("id") int systemId) {
        log.info("about to fetch planets from planetary system with id: " + systemId);
        PlanetarySystem ps = service.getSystemById(systemId);
        return ps != null ? ResponseEntity.ok(service.getPlanets(ps)) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body("Object not found.");
    }


    @PostMapping
    public ResponseEntity<?> addSystem(@Validated @RequestBody PlanetarySystem system, Errors errors){
        log.info("about to add system " + system);
        if(errors.hasErrors()){
            log.severe("validation errors");
            StringBuilder builder = new StringBuilder("validation errors:\n");
            errors.getAllErrors().forEach(e->builder.append("error:" + e.getCode()+"\n"));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(builder.toString());
        }
        system = service.addPlanetarySystem(system);
        return ResponseEntity.status(HttpStatus.CREATED).body(system);
    }




}
