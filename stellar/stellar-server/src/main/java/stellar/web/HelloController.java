package stellar.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(Model model, @RequestParam(value = "suffix", required = false) String suffix){
        suffix = suffix!=null ? suffix : "xyz";
        model.addAttribute("hello_text", "It's Wednesday! " + suffix);
        return "hello";
    }

}
