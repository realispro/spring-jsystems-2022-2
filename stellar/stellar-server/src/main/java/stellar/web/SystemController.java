package stellar.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.config.Adviced;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Adviced
@Controller
@RequiredArgsConstructor
public class SystemController {

    private Logger log = Logger.getLogger(SystemController.class.getName());

    private final StellarService service;
    private final SystemValidator validator;

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @GetMapping("/systems")
    public String getSystems(
            Model model,
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @RequestHeader Map<String, String> headers,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId,
            HttpServletResponse response
            ){

        if(phrase!=null && phrase.trim().equals("throw")){
            throw new IllegalArgumentException("mocking server side problem");
        }

        log.info("session id: " + sessionId);
        log.info("user agent header: " + userAgent);
        headers.entrySet().forEach(e->log.info("header entry: " + e.getKey() + ":" + e.getValue()));

        List<PlanetarySystem> systems = phrase==null ? service.getSystems() : service.getSystemsByName(phrase);
        model.addAttribute("systems", systems);

        Cookie cookie = new Cookie("stellar-cookie", "stellar-value");
        cookie.setMaxAge(60*1);
        response.addCookie(cookie);

        return "systems";
    }

    @GetMapping("/addSystem")
    public String addSystemPrepare(Model model){

        model.addAttribute("systemForm", new PlanetarySystem());

        return "addSystem";
    }

    @PostMapping("/addSystem")
    public String addSystem(@Validated @ModelAttribute("systemForm") PlanetarySystem system, Errors errors){

        log.info("about to add planetary system: " + system);

        if(errors.hasErrors()){
            return "addSystem";
        }

        service.addPlanetarySystem(system);

        return "redirect:/systems";
    }

}
