package stellar.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import stellar.config.Adviced;
import stellar.model.Planet;
import stellar.service.StellarService;

import java.util.List;

@Adviced
@Controller
@RequiredArgsConstructor
public class PlanetController {

    private final StellarService service;

    @GetMapping("/planets")
    public String getPLanets(Model model, @RequestParam(value = "systemId", required = true) int systemId){

        List<Planet> planets = service.getPlanets(service.getSystemById(systemId));
        model.addAttribute("planets", planets);
        return "planets";
    }
}