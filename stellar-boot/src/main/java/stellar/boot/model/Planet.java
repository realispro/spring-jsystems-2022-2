package stellar.boot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/*@Getter
@Setter
@ToString*/
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name="planet")
public class Planet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int size;
    private int weight;
    private int moons;

    @ManyToOne
    @JoinColumn(name = "system_id")
    private PlanetarySystem system; // system_id

    public Planet(int id, String name, PlanetarySystem system) {
        this(id, name, 0, 0, 0, system);
    }

}
