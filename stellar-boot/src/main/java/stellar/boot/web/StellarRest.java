package stellar.boot.web;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import stellar.boot.dao.SystemRepository;
import stellar.boot.model.PlanetarySystem;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class StellarRest {

    private final SystemRepository repository;

    @GetMapping("/systems")
    public List<PlanetarySystem> getSystems(){
        return repository.findAll();
    }    
}
